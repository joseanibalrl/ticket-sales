package com.transporte.urbano.model;

import java.sql.Time;
import java.util.List;

public class Destination {
    // Identificador del destino
    private Integer id;
    // Nombre del destino
    private String name;
    // Costo de viaje del destino
    private Double price;
    // Minutos que toma llegar a esta parada
    private Integer minutes;
    // Lista de horarios de salidas de este destino
    private List<Time> times;
    // Destino siguiente
    private Destination next;
    // Destino previo
    private Destination previous;

    // Constructor privado
    private Destination(Integer id, String name, Double price, Integer minutes, List<Time> times) {
        this.name = name;
        this.price = price;
        this.minutes = minutes;
        this.times = times;
        this.id = id;
    }

    // Inicializador estático para evitar llamar al constructor desde fuera
    public static Destination of(Integer id, String name, Double cost, Integer minutes, List<Time> times) {
        return new Destination(id, name, cost, minutes, times);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Destination getNext() {
        return next;
    }

    public Destination setNext(Destination next) {
        // Asigna el destino siguiente
        this.next = next;
        // Asigna como previo del siguiente a este destino
        next.setPrevious(this);
        // Retorna esta instancia para evitar multiples lineas
        return this;
    }

    public Destination getPrevious() {
        return previous;
    }

    public void setPrevious(Destination previous) {
        this.previous = previous;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public List<Time> getTimes() {
        return times;
    }

    public void setTimes(List<Time> times) {
        this.times = times;
    }

    @Override
    public String toString() {
        return name;
    }
}
