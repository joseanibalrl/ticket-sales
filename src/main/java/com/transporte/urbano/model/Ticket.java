package com.transporte.urbano.model;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Objects;

public class Ticket {
    // Pasajero
    private Passenger passenger;
    // Parada de salida
    private Destination from;
    // Destino final
    private Destination to;
    // Hora de salida
    private Time time;

    // Getter y setters

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Destination getFrom() {
        return from;
    }

    public void setFrom(Destination from) {
        this.from = from;
    }

    public void setTo(Destination to) {
        this.to = to;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    @Override
    public String toString() {
        // Imprime el detalle del ticket
        return "Ticket{" +
                "Pasajero=" + passenger.getFullName() +
                ", Horario=" + time +
                ", Desde=" + from +
                ", Hasta=" + to +
                ", Precio=" + this.getPrice() +
                ", Hora de llegada=" + getArrival() +
                '}';
    }

    private Double getPrice() {
        // Valida si va o viene
        if (to.getId() > from.getId()) {
            Destination next = from.getNext();
            // Asigna el valor de la siguiente parada al precio inicial
            Double price = next.getPrice();
            while (true) {
                // Valida si ya llego a la parada que deseaba ir
                if (next.getId().equals(to.getId())) {
                    break;
                }
                // Se mantiene asignando el precio de cada parada hasta llegar al destino
                if (Objects.nonNull(next.getNext())) {
                    next = next.getNext();
                    // Actualiza el precio con el precio de la parada actual
                    price += next.getPrice();
                    continue;
                }
                break;
            }
            return price;
        } else {
            Destination previous = from.getPrevious();
            Double price = previous.getPrice();
            while (true) {
                // Valida si ya llego a la parada que deseaba ir
                if (previous.getId().equals(to.getId())) {
                    break;
                }
                // Se mantiene asignando el precio de cada parada hasta llegar al destino
                if (Objects.nonNull(previous.getPrevious())) {
                    previous = previous.getPrevious();
                    // Actualiza el precio con el precio de la parada actual
                    price += previous.getPrice();
                    continue;
                }
                break;
            }
            return price;
        }
    }

    private Time getArrival() {
        Integer minutes;
        // Valida si va o viene
        if (to.getId() > from.getId()) {
            Destination next = from.getNext();
            // Asigna el valor de la siguiente parada al precio inicial
            minutes = next.getMinutes();
            while (true) {
                // Valida si ya llego a la parada que deseaba ir
                if (next.getId().equals(to.getId())) {
                    break;
                }
                // Se mantiene asignando el precio de cada parada hasta llegar al destino
                if (Objects.nonNull(next.getNext())) {
                    next = next.getNext();
                    // Actualiza el precio con el precio de la parada actual
                    minutes += next.getMinutes();
                    continue;
                }
                break;
            }
        } else {
            Destination previous = from.getPrevious();
            minutes = previous.getMinutes();
            while (true) {
                // Valida si ya llego a la parada que deseaba ir
                if (previous.getId().equals(to.getId())) {
                    break;
                }
                // Se mantiene asignando el precio de cada parada hasta llegar al destino
                if (Objects.nonNull(previous.getPrevious())) {
                    previous = previous.getPrevious();
                    // Actualiza el precio con el precio de la parada actual
                    minutes += previous.getMinutes();
                    continue;
                }
                break;
            }
        }
        Time time = this.time;
        // Agregamos los minutos al tiempo de salida.
        LocalTime localTime = time.toLocalTime().plusMinutes(minutes);
        // Retornamos el tiempo de llegada
        return Time.valueOf(localTime);
    }
}
