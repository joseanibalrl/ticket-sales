package com.transporte.urbano.model;

import com.transporte.urbano.program.Messages;

public class Passenger {
    // Nombre del pasajero
    private String firstname;
    // Apellido del pasajero
    private String lastname;
    // Matrícula del pasajero
    private String collegeId;
    // Dirección del pasajero
    private String address;
    // Teléfono del pasajero
    private String phone;

    // Constructor privado
    private Passenger(String firstname, String lastname, String collegeId, String address, String phone) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.collegeId = collegeId;
        this.address = address;
        this.phone = phone;
    }

    // Inicializador estático para evitar llamar al constructor desde fuera
    public static Passenger of(String firstname, String lastname, String document, String address, String phone) {
        return new Passenger(firstname, lastname, document, address, phone);
    }

    // Getters y setters

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(String collegeId) {
        this.collegeId = collegeId;
    }

    @Override
    public String toString() {
        // Muestra el detalle del pasajero en línea.
        return String.format("{" +
                        "%s='" + firstname + '\'' +
                        ", %s='" + lastname + '\'' +
                        ", %s='" + collegeId + '\'' +
                        ", %s='" + address + '\'' +
                        ", %s='" + phone + '\'' +
                        '}',
                Messages.first_name,
                Messages.last_name,
                Messages.college_id,
                Messages.address,
                Messages.phone_number);
    }

    public String getFullName() {
        // Muestra el nombre completo del pasajero
        return String.format("%s %s", this.getFirstname(), this.getLastname());
    }
}
