package com.transporte.urbano.actions;

import com.transporte.urbano.model.Destination;
import com.transporte.urbano.model.Passenger;
import com.transporte.urbano.model.Ticket;
import com.transporte.urbano.program.Destinations;
import com.transporte.urbano.program.Messages;
import com.transporte.urbano.program.Option;
import com.transporte.urbano.program.Passengers;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TicketSaleAction extends Action {

    private final Ticket ticket = new Ticket();

    @Override
    public Option init() {
        // Llama a elegir el pasajero
        final Option passengerOption = this.prompForPassenger();
        // En caso de elegir volver, retorna la opción de volver
        if (passengerOption.isExit()) {
            return passengerOption;
        }
        // Llama a seleccionar los destinos (Inicio y fin de viaje)
        final Option destinationOption = this.prompForDestinations();
        // En caso de elegir volver, retorna la opción de volver
        if (destinationOption.isExit()) {
            return destinationOption;
        }
        // Muestra la información del ticket
        Messages.display(ticket.toString());
        // Pregunta por las opciones principales (Salir o Comprar ticket)
        final Option option = super.ask();
        // En caso de elegir volver, retorna la opción de volver
        if (option.isExit()) {
            return option;
        }
        // Retorna la acción seleccionada y la inicializa en orden
        // de mantener la ejecución de esta pantalla, cuando se salga de la otra
        return option.geAction().init();
    }

    @Override
    public List<Option> options() {
        // Opciones de pantalla
        return Arrays.asList(
                // Volver a pantalla anterior
                Option.of(Action.exit, Messages.system_back),
                // Comprar ticket
                Option.of(Action.first, Messages.buy, new BuyTicketAction()));
    }

    private Option prompForPassenger() {
        // Opción de salir (Volver en este caso)
        final List<Option> options = this.getExitOption();
        // Selecciona los pasajeros guardados
        final List<Passenger> passengers = Passengers.passengers;
        // Itera sobre los pasajeros para agregar la opción de seleccionarlos
        for (int i = 0; i < passengers.size(); i++) {
            // Obtiene la información del pasajero para el índice actual.
            final Passenger passenger = passengers.get(i);
            // Crea la opción del menu con la información del pasajero.
            options.add(Option.of(Integer.toString(i + 1), passenger.toString(), new TicketSaleAction()));
        }
        // Muestra mensaje de seleccionar pasajero
        Messages.display(Messages.select_passenger);
        // Muestra las opciones a seguir
        final Option option = super.ask(options);
        // En caso de elegir volver, retorna la opción de volver
        if (option.isExit()) {
            return option;
        }
        // Toma el índice seleccionado
        final Integer index = option.getKeyInt();
        // Toma el pasajero seleccionado partiendo del índice anterior
        final Passenger passenger = passengers.get(index - 1);
        // Asigna el pasajero al ticket
        this.ticket.setPassenger(passenger);
        // Retorna la opción para mantener pantalla activa o salir en caso de
        return option;
    }

    private Option prompForDestinations() {
        // Llama a seleccionar la parada de inicio
        final Option startOption = this.prompForStart();
        // En caso de elegir volver, retorna la opción de volver
        if (startOption.isExit()) {
            return startOption;
        }
        // Llama a seleccionar la parada de fin
        // En caso de elegir volver, retorna la opción de volver
        return this.prompForEnd();
    }

    private Option prompForEnd() {
        // Carga option de salir al menu principal
        final List<Option> options = this.getExitOption();
        // Carga destinos disponibles
        final List<Destination> destinations = Destinations.getDestinations();
        // Iteramos sobre los destinos para cargar el menu
        for (final Destination destination : destinations) {
            // Genera option del menu para cada destino
            options.add(Option.of(Integer.toString(destination.getId()), destination.toString(), new TicketSaleAction()));
        }
        // Mostramos mensaje de seleccionar destino final
        Messages.display(Messages.end);
        // Muestra las opciones
        final Option option = super.ask(options);
        // Si el usuario selecciona salir, debe permitir salir
        if (option.isExit()) {
            return option;
        }
        // Tomamos el índice de la opción seleccionada y asignamos el destino que fue seleccionado
        final Integer toIndex = option.getKeyInt();
        final Destination to = destinations.get(toIndex - 1);
        // Si es el mismo destino de inicio, no debe permitir comprar el ticket
        if (to.getId().equals(this.ticket.getFrom().getId())) {
            Messages.display("El destino debe ser diferente a la parada en que se encuentra");
            // Enviamos a seleccionar destino final, dado que es el mismo del inicial
            return this.prompForEnd();
        }
        // Asignamos el destino final
        this.ticket.setTo(to);
        // Retornamos la opción seleccionada
        return option;
    }

    private Option prompForStart() {
        // Carga destinos disponibles
        final List<Destination> destinations = Destinations.getDestinations();
        // Carga option de salir al menu principal
        final List<Option> options = this.getExitOption();
        // Iteramos sobre los destinos para cargar el menu
        for (final Destination destination : destinations) {
            // Genera option del menu para cada destino
            options.add(Option.of(Integer.toString(destination.getId()), destination.toString(), new TicketSaleAction()));
        }
        // Mostramos mensaje de seleccionar destino final
        Messages.display(Messages.start);
        // Muestra las opciones
        final Option option = super.ask(options);
        // Si el usuario selecciona salir, debe permitir salir
        if (option.isExit()) {
            return option;
        }
        // Tomamos el índice de la opción seleccionada y asignamos el destino que fue seleccionado
        final Integer fromIndex = option.getKeyInt();
        final Destination from = destinations.get(fromIndex - 1);

        // Carga option de salir al menu principal
        final List<Option> scheduledOptions = this.getExitOption();
        // Itera sobre los horarios que tiene la terminal seleccionada
        for (int i = 0; i < from.getTimes().size(); i++) {
            // Toma el horario para el índice actual
            final Time time = from.getTimes().get(i);
            // Genera option del menu para cada horario
            scheduledOptions.add(Option.of(Integer.toString(i + 1), time.toString(), new TicketSaleAction()));
        }
        // Mostramos mensaje de seleccionar horario de salida
        Messages.display(Messages.chose_schedule);
        // Muestra las opciones
        final Option scheduleOption = super.ask(scheduledOptions);
        // Si el usuario selecciona salir, debe permitir salir
        if (scheduleOption.isExit()) {
            return scheduleOption;
        }
        // Agrega al ticket la información de la parada de salida
        this.ticket.setFrom(from);
        // Agrega al ticket la información de la hora de salida
        this.ticket.setTime(Time.valueOf(scheduleOption.getOption()));

        return scheduleOption;
    }

    private List<Option> getExitOption() {
        // Retorna listado con un solo valor el cual contiene la opción de salir
        final List<Option> exitOption = Collections.singletonList(Option.of(Action.exit, Messages.system_back));
        return new ArrayList<>(exitOption);
    }

}
