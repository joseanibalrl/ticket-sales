package com.transporte.urbano.actions;

import com.transporte.urbano.program.Messages;
import com.transporte.urbano.program.Option;

import java.util.Arrays;
import java.util.List;

public class BuyTicketAction extends Action {
    @Override
    public Option init() {
        // Muestra mensaje de que el ticket fue comprado
        Messages.display(Messages.ticket_bought);
        // Muestra las opciones a seguir
        final Option option = super.ask();
        // En caso de elegir volver, retorna la opción de volver
        if (option.isExit()) {
            return option;
        }
        // Retorna la siguiente opción (Comprar otro ticket) y envia a la pantalla de compra
        return option.geAction().init();
    }

    @Override
    public List<Option> options() {
        return Arrays.asList(
                Option.of(Action.exit, Messages.system_back),
                Option.of(Action.first, Messages.buy_another, new TicketSaleAction()));
    }
}
