package com.transporte.urbano.actions;

import com.transporte.urbano.program.Messages;
import com.transporte.urbano.program.Option;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public abstract class Action {
    // Opciones del menú
    public static final String exit = "0";
    public static final String first = "1";
    public static final String second = "2";
    public static final String third = "3";

    // Scanner de terminal
    private final Scanner scanner = new Scanner(System.in);

    // Método abstracto que inicializa menú
    public abstract Option init();

    // Opciones del menu de cada pantalla
    public abstract List<Option> options();

    // Pide la opción siguiente al usuario
    protected Option ask() {
        return ask(this.options());
    }

    // Pide la opción siguiente al usuario
    protected Option ask(final List<Option> options) {
        final String message = options.stream().map(Option::getMessage).collect(Collectors.joining("\n"));
        Messages.display(message);
        final String text = this.getInput();
        final Optional<Option> option = options.stream().filter(o -> o.getKey().equals(text)).findFirst();
        if (option.isPresent()) {
            return option.get();
        }
        Messages.display(Messages.invalid_option);
        return this.ask(options);
    }

    // Pide la opción siguiente al usuario
    protected String ask(String message) {
        Messages.display(String.format("%s: ", message), Boolean.TRUE);
        final String line = this.scanner.nextLine();
        return line.trim();
    }

    // Retorna el string escrito en la consola
    private String getInput() {
        // Despliega el mensaje de seleccionar data
        Messages.display(Messages.input_selection, Boolean.TRUE);
        final String line = this.scanner.nextLine();
        Messages.display(Messages.divider, Boolean.TRUE);
        return line;
    }
}
