package com.transporte.urbano.actions;

import com.transporte.urbano.model.Passenger;
import com.transporte.urbano.program.Messages;
import com.transporte.urbano.program.Option;

import java.util.Arrays;
import java.util.List;

public class RegisterAction extends Action {

    @Override
    public Option init() {
        // Ingreso del nombre
        String firstname = super.ask(Messages.first_name);
        // Ingreso del apellido
        String lastname = super.ask(Messages.last_name);
        // Ingreso de la matricula
        String collegeId = super.ask(Messages.college_id);
        // Ingreso de la dirección
        String address = super.ask(Messages.address);
        // Ingreso del teléfono
        String phone = super.ask(Messages.phone_number);
        // Crea objeto de pasajero y pasa los parámetros recolectados como argumentos.
        Passenger passenger = Passenger.of(firstname, lastname, collegeId, address, phone);
        // Inicializa la pantalla de guardar pasajero
        final Option saveResult = new SavePassengerAction(passenger).init();
        // En caso de cancelar la operación, se envía a la pantalla anterior.
        if (saveResult.isExit()) {
            // Se muestra mensaje de que el pasajero no fue guardado
            Messages.display(Messages.passenger_not_saved);
            // En caso de elegir volver, retorna la opción de volver
            return saveResult;
        }
        // Muestra las opciones a seguir
        final Option option = super.ask();
        // En caso de elegir volver, retorna la opción de volver
        if (option.isExit()) {
            return option;
        }
        // En caso de volver a crear un nuevo pasajero, se llama al método recursivamente
        return init();
    }

    @Override
    public List<Option> options() {
        // Opciones de pantalla
        return Arrays.asList(
                // Volver a pantalla anterior
                Option.of(Action.exit, Messages.system_back),
                // Envía a registrar otro pasajero
                Option.of(Action.first, Messages.register, new RegisterAction())
        );
    }
}
