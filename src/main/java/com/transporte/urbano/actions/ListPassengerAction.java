package com.transporte.urbano.actions;

import com.transporte.urbano.program.Messages;
import com.transporte.urbano.program.Option;
import com.transporte.urbano.program.Passengers;

import java.util.Arrays;
import java.util.List;

public class ListPassengerAction extends Action {
    public static void showPassengers() {
        // Itera sobre el listado de pasajeros y los muestra en pantalla
        for (int i = 0; i < Passengers.passengers.size(); i++) {
            // Muestra el listado de pasajeros en pantalla
            Messages.display(String.format("(%d): %s", i + 1, Passengers.passengers.get(i)));
        }
    }

    @Override
    public Option init() {
        // Muestra mensaje divisor
        Messages.divider();
        // Muestra los pasajeros registrados
        ListPassengerAction.showPassengers();
        // Muestra las opciones a seguir
        final Option option = super.ask();
        // En caso de elegir volver, retorna la opción de volver
        if (option.isExit()) {
            return option;
        }
        // Retorna la siguiente opción (Listar los pasajeros) y envía a la pantalla de listar pasajeros
        return option.geAction().init();
    }

    @Override
    public List<Option> options() {
        // Opciones de la vista
        return Arrays.asList(
                // Opción de salir (Volver en este caso)
                Option.of(Action.exit, Messages.system_back),
                // Vuelve a listar las opciones
                Option.of(Action.first, Messages.list_again, new ListPassengerAction()));
    }
}
