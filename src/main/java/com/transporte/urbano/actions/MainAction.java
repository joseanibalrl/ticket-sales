package com.transporte.urbano.actions;

import com.transporte.urbano.program.Messages;
import com.transporte.urbano.program.Option;

import java.util.Arrays;
import java.util.List;

public class MainAction extends Action {

    @Override
    public Option init() {
        // Muestra el mensaje de elegir opción en el inicio del programa
        Messages.display(Messages.ask_for_task);
        // Muestra las opciones a seguir
        final Option option = super.ask();
        // En caso de elegir volver, retorna la opción de volver
        if (option.isExit()) {
            return option;
        }
        // Muestra la opción seleccionada
        Messages.display(String.format(Messages.you_choose, option.getOption()));
        // Se inicializa la acción seleccionada
        option.geAction().init();
        // Se mantiene el programa en ejecución al utilizar recursividad
        return init();
    }

    @Override
    public List<Option> options() {
        // Opciones de la pantalla
        return Arrays.asList(
                // Salir del sistema (Terminar proceso)
                Option.of(Action.exit, Messages.system_exit),
                // Primera opción, registrar un pasajero
                Option.of(Action.first, Messages.register, new RegisterAction()),
                // Segunda opción, listar los pasajeros registrados
                Option.of(Action.second, Messages.list_passengers, new ListPassengerAction()),
                // Tercera opción, venta de ticket
                Option.of(Action.third, Messages.ticket_sale, new TicketSaleAction())
        );
    }
}
