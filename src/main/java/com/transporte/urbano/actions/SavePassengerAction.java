package com.transporte.urbano.actions;

import com.transporte.urbano.model.Passenger;
import com.transporte.urbano.program.Messages;
import com.transporte.urbano.program.Option;
import com.transporte.urbano.program.Passengers;

import java.util.Arrays;
import java.util.List;

public class SavePassengerAction extends Action {
    private final Passenger passenger;

    public SavePassengerAction(Passenger passenger) {
        this.passenger = passenger;
    }

    @Override
    public Option init() {
        // Muestra mensaje divisor
        Messages.divider();
        // Muestra mensaje con la informacion del pasajero antes de ser guardado
        Messages.display("Pasajero: ", passenger.toString());
        // Muestra las opciones a seguir
        final Option option = super.ask();
        // En caso de elegir volver, retorna la opción de volver
        if (option.isExit()) {
            return option;
        }
        // Guarda el pasajero en el listado de pasajeros que se tiene
        Passengers.passengers.add(this.passenger);
        // Muestra mensaje de que el pasajero fue guardado exitosamente
        Messages.display(Messages.saved);
        // Retorna la opción seleccionada
        return option;
    }

    @Override
    public List<Option> options() {
        // Opciones de pantalla
        return Arrays.asList(
                // Volver a pantalla anterior
                Option.of(Action.exit, Messages.system_back),
                // Guarda el pasajero y envía a la pantalla de registro
                Option.of(Action.first, Messages.save, new RegisterAction()));
    }
}
