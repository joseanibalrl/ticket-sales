package com.transporte.urbano.program;

import com.transporte.urbano.model.Destination;

import java.sql.Time;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Destinations {
    public static List<Destination> getDestinations() {
        // Primera parada
        final List<Time> firstTimes = Destinations.times("08:00", "11:15", "14:30"); // Horarios
        final Destination first = Destination.of(1, "Primera Parada", 20.0, 10, firstTimes);
        // Segunda parada
        final List<Time> secondTimes = Destinations.times("08:40", "11:55", "15:10"); // Horarios
        final Destination second = Destination.of(2, "Segunda Parada", 10.0, 40, secondTimes);
        // Tercera parada
        final List<Time> thirdTimes = Destinations.times("09:30", "12:45", "16:00"); // Horarios
        final Destination third = Destination.of(3, "Tercera Parada", 15.0, 50, thirdTimes);
        // Cuarta parada
        final List<Time> fourthTimes = Destinations.times("10:00", "12:15", "16:30"); // Horarios
        final Destination fourth = Destination.of(4, "Cuarta Parada", 20.0, 30, fourthTimes);
        // Vincula las paradas vecinas
        first.setNext(second.setNext(third.setNext(fourth)));
        // Retorna listado de las paradas
        return Arrays.asList(first, second, third, fourth);
    }

    private static List<Time> times(String... times) {
        // Convierte Strings con los horarios a {@link java.sql.Time}
        return Arrays.stream(times).map(t -> Time.valueOf(t + ":00")).collect(Collectors.toList());
    }
}
