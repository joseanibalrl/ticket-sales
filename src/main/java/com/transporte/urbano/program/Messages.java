package com.transporte.urbano.program;

public class Messages {
    public static final String invalid_option = "--- Opción no permitida ---";
    public static final String you_choose = "Ha elegido (%s).";
    public static final String welcome = "Bienvenido/a a Transporte Urbano";
    public static final String ask_for_task = "¿Qué operación desea realizar?";
    public static final String divider = "-------------------------------------------\n";
    public static final String input_selection = "Ingrese su selección: ";
    public static final String system_exit = "Salir del programa";
    public static final String system_back = "Volver al Menú Principal";
    public static final String register = "Registrar Pasajero";
    public static final String save = "Guardar Pasajero";
    public static final String buy = "Comprar Ticket";
    public static final String buy_another = "Comprar Otro Ticket";
    public static final String ticket_bought = "Ticket Comprado Exitosamente.";
    public static final String list_again = "Volver a Listar los Pasajeros";
    public static final String saved = "Pasajero Guardado Exitosamente";
    public static final String list_passengers = "Listar Pasajeros Guardados";
    public static final String ticket_sale = "Venta de Ticket";
    public static final String first_name = "Nombre(s)";
    public static final String last_name = "Apellido(s)";
    public static final String phone_number = "Teléfono";
    public static final String address = "Dirección";
    public static final String college_id = "Matrícula";
    public static final String passenger_not_saved = "Pasajero no Guardado";
    public static final String end = "Por favor, seleccione a donde se dirige: ";
    public static final String start = "Por favor, seleccione donde se encuentra: ";
    public static final String chose_schedule = "Por favor, seleccione el horario de salida";
    public static final String select_passenger = "Por favor, seleccione el pasajero: ";

    public static void display(String leading, String message) {
        // Muestra mensaje con texto fijo delante
        final String msg = leading + message;
        // Llama a mostrar mensaje, indicando que no será en la misma línea
        Messages.display(msg, Boolean.FALSE);
    }

    public static void display(String message) {
        // Llama a mostrar mensaje, indicando que no será en la misma línea
        Messages.display(message, Boolean.FALSE);
    }

    public static void display(String message, Boolean inline) {
        // Verifica si el mensaje debe ser en la misma linea
        if (inline) {
            // Muestra el mensaje sin salto de línea
            System.out.println(message);
        } else {
            // Muestra el mensaje con salto de línea
            System.out.println(message);
            // Agrega divisor a la pantalla
            Messages.divider();
        }
    }

    public static void divider() {
        // Muestra el divisor
        System.out.println(Messages.divider);
    }
}
