package com.transporte.urbano.program;

import com.transporte.urbano.model.Passenger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Passengers {
    // Base de pasajeros. En esta variable se almacenan todos los pasajeros registrados en el sistema.
    public static List<Passenger> passengers = new ArrayList<>(Arrays.asList(
            Passenger.of("Octavio Enrique", "Severino Castillo", "2012-0796", "Dirección de Octavio", "809-111-1111"),
            Passenger.of("Deybi Alejandro", "Ramirez Alfonseca", "2011-364", "Dirección de Deybi", "809-222-2222"),
            Passenger.of("Luis Manuel", "Henríquez Ventura", "2016-0993", "Dirección de Luis Manuel", "809-333-3333")
    ));
}
