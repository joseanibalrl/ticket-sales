package com.transporte.urbano.program;

import com.transporte.urbano.actions.Action;

import java.util.Objects;

public class Option {
    // Key (Tecla) que debe ser oprimida para seleccionar opción
    private final String key;
    // Nombre de la opción mostrada
    private final String option;
    // Action (Pantalla) que se mostrara al momento de seleccionar la opción
    private final Action action;

    // Constructor privado
    private Option(String key, String option, Action action) {
        this.key = key;
        this.option = option;
        this.action = action;
    }

    // Inicializador estático para evitar llamar al constructor desde fuera
    public static Option of(String key, String option, Action action) {
        return new Option(key, option, action);
    }

    // Inicializador estático para evitar llamar al constructor desde fuera
    public static Option of(String key, String option) {
        return new Option(key, option, null);
    }

    public String getKey() {
        return key;
    }

    public String getOption() {
        return option;
    }

    public Action geAction() {
        return action;
    }

    public boolean isExit() {
        return Objects.isNull(action);
    }

    public String getMessage() {
        // Muestra opcion en pantalla en formato (key) - Opción
        return String.format("(%s) - %s", this.getKey(), this.getOption());
    }

    public Integer getKeyInt() {
        try {
            // Intenta parsear key a número, para obtener el índice de la opción seleccionada.
            return Integer.parseInt(key);
        } catch (NumberFormatException ignored) {
            // Retorna null en caso de no parsear el índice.
            return null;
        }
    }
}
