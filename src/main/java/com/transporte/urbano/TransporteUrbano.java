package com.transporte.urbano;

import com.transporte.urbano.actions.MainAction;
import com.transporte.urbano.program.Messages;
import com.transporte.urbano.program.Option;

public class TransporteUrbano {

    public static void main(String[] args) {
        // Damos la bienvenida al sistema
        Messages.display(Messages.welcome);
        // Crea el objeto de Action principal y lo inicializa
        final MainAction action = new MainAction();
        final Option init = action.init();
        // Muestra opción elegida antes de salir del sistema
        Messages.display(String.format(Messages.you_choose, init.getOption()));
    }
}
